public class Regiments {
      String name;
      double morale ;
      int discipline;
      int numberOfSoldiers;
      int dmgToPop;
      int dmgToMorale;

      public void regiment(String name, double morale, int discipline, int numberOfSoldiers, int dmgToPop, int dmgToMorale){
            this.name = name;
            this.morale = morale;
            this.discipline = discipline;
            this.numberOfSoldiers = numberOfSoldiers;
            this.dmgToPop=dmgToPop;
            this.dmgToMorale=dmgToMorale;
      }

      public String toString(){
            return("name: " + name + ", morale: " + morale + ", discipline: " + discipline + ", Soldiers: " + numberOfSoldiers);
      }

      public Regiments wasAttacked(Regiments whoDef, Regiments whoAt){
            this.morale-=whoAt.dmgToMorale;
            this.numberOfSoldiers-=whoAt.dmgToPop*this.discipline/100;
            whoAt.numberOfSoldiers-=this.dmgToPop;
            whoAt.morale -= whoDef.dmgToMorale/2;
            return(whoAt);
      }
}
