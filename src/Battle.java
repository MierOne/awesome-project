public class Battle {
      public static void main(String[] args){
            Regiments Roman = new Regiments();
            Regiments Gallic = new Regiments();
            Regiments Greek = new Regiments();

            Roman.regiment("Roman", 350, 130, 4000, 400, 20);
            Gallic.regiment("Gallic", 367, 87, 6450, 300, 35);
            Greek.regiment("Greek", 334, 111, 4750, 370, 26);

            System.out.println(Roman);
            System.out.println(Greek);
            System.out.println(Gallic);
            System.out.println(" ");

            Gallic = Roman.wasAttacked(Roman, Gallic);

            Roman = Gallic.wasAttacked(Gallic, Roman);

            Greek = Gallic.wasAttacked(Gallic, Greek);

            System.out.println(Roman);
            System.out.println(Greek);
            System.out.println(Gallic);
            System.out.println(" ");
      }
}
