public class Calculate {
      Calculator multiplication = ((x,y) -> (x*y));
      Calculator minus = ((param1, param2) -> (param1 - param2));

      public Integer calculate(Integer i1, Integer i2, Calculator calculate) {
            return calculate.operationWithTwoInteger(i1, i2);
      }

      @FunctionalInterface
      public interface Calculator {
            Integer operationWithTwoInteger(Integer param1, Integer param2);
      }
}
