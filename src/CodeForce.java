import java.util.ArrayList;
import java.util.ArrayDeque;
import javafx.util.Pair;
import java.util.Scanner;

public class CodeForce {
      public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int numOfDataset = scanner.nextInt();
            for (int i = 0; i < numOfDataset; i++) {
                  int numOfSongs = scanner.nextInt();
                  int countForDelete = 0;
                  ArrayDeque<Integer> arrayDequeSongsForDelete = new ArrayDeque<>();
                  int index = 1;
                  boolean isDeletedSomeSong = false;

                  ArrayList<Pair<Integer, Integer>> arrayList = new ArrayList<>();
                  for (int j = 0; j < numOfSongs; j++) {
                        arrayList.add(new Pair<>(scanner.nextInt(), j+1));
                  }
                  while (arrayList.size() > 1) {
                        if(index == 0){
                              if(gcd(arrayList.get(0).getKey(), arrayList.get(arrayList.size()-1).getKey()) == 1){
                                    countForDelete++;
                                    arrayDequeSongsForDelete.add(arrayList.get(0).getValue());
                                    arrayList.remove(0);
                                    index++;
                                    isDeletedSomeSong = true;
                              } else {
                                    isDeletedSomeSong = false;
                                    index++;
                              }
                              continue;
                        }
                        if(index == arrayList.size()-1){
                              if(gcd(arrayList.get(index).getKey(), arrayList.get(index-1).getKey()) == 1){
                                    countForDelete++;
                                    arrayDequeSongsForDelete.add(arrayList.get(index).getValue());
                                    arrayList.remove(index);
                                    isDeletedSomeSong = true;
                              }
                              if(!isDeletedSomeSong){
                                    break;
                              }
                              index = 0;
                              continue;
                        }
                        if(gcd(arrayList.get(index).getKey(), arrayList.get(index-1).getKey()) == 1){
                              countForDelete++;
                              arrayDequeSongsForDelete.addLast(arrayList.get(index).getValue());
                              arrayList.remove(index);
                              isDeletedSomeSong = true;
                        }
                        index++;
                  }
                  if(arrayList.size() == 1 && arrayList.get(0).getKey()==1){
                        countForDelete++;
                        arrayDequeSongsForDelete.add(arrayList.get(0).getValue());
                  }

                  System.out.print(countForDelete + " ");
                  while(arrayDequeSongsForDelete.size()!=0) {
                        System.out.print(arrayDequeSongsForDelete.removeFirst() + " ");
                  }
                  System.out.println();
            }
      }

      public static int gcd(int a, int b) {
            if (b == 0) return a;
            return gcd(b, a % b);
      }
}